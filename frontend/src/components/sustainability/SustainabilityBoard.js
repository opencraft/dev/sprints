import React, {Component} from "react";
import DatePicker from "react-datepicker";
import SustainabilityTable from "./SustainabilityTable";
import {connect} from "react-redux";
import {auth, sustainability} from "../../actions";
import {COMPANY_NAME, SUSTAINABILITY_DASHBOARD_DOCS} from "../../constants";
import "react-datepicker/dist/react-datepicker.css";

class SustainabilityBoard extends Component {
    constructor(props) {
        super(props);

        this.state = {
            startDate: this.props.sustainability.startDate,
            endDate: this.props.sustainability.endDate,
        };
    }

    // Because everyone counts months from 0.
    dateString = (d) => `${d.getFullYear()}-${d.getMonth() + 1}-${d.getDate()}`;

    loadAccounts = () => this.props.loadAccounts(this.dateString(this.state.startDate), this.dateString(this.state.endDate));

    componentDidMount() {
        this.loadAccounts();
    }

    transformBudgets = (accountsObj) => {
        // Transforming budget data for faster lookup by account name
        const transformedData = {};

        const transformArray = (array, type) => {
            array.forEach(account => {
                transformedData[account.name] = {
                    ...account,
                    account_type: type,
                };
            });
        };

        transformArray(accountsObj.billable_accounts, 'billable');
        transformArray(accountsObj.non_billable_accounts, 'non_billable');
        transformArray(accountsObj.non_billable_responsible_accounts, 'non_billable_responsible');

        return transformedData;
    };

    prepareData(accounts, budgets, range, id) {
        if (!accounts || !Object.keys(accounts).length) {
            return {};
        }

        const data = {};
        const selectedAccounts = this.props.selectedAccounts;
        const transformedBudgets = this.transformBudgets(budgets);

        if (range === 'board') {
            try {
                const project_name = this.viewName(range, id);
                data.billable = accounts.billable_accounts.by_project[project_name] || 0;
                data.non_billable = accounts.non_billable_accounts.by_project[project_name] || 0;
                data.non_billable_responsible = accounts.non_billable_responsible_accounts.by_project[project_name] || 0;
            }
            catch (e) {
                // Cell's board not loaded.
            }
        } else if (range === 'user_board') {
            data.billable = accounts.billable_accounts.by_person[id] || 0;
            data.non_billable = accounts.non_billable_accounts.by_person[id] || 0;
            data.non_billable_responsible = accounts.non_billable_responsible_accounts.by_person[id] || 0;
        } else {
            data.billable = accounts.billable_accounts.overall || 0;
            data.non_billable = accounts.non_billable_accounts.overall || 0;
            data.non_billable_responsible = accounts.non_billable_responsible_accounts.overall || 0;
        }

        // Iterate over selectedAccounts and subtract 'overall' if an account is false, depending on the range
        Object.keys(selectedAccounts).forEach(accountName => {
            if (!selectedAccounts[accountName] && transformedBudgets[accountName]) {
                const account = transformedBudgets[accountName];

                // Handle subtraction based on range
                if (range === 'board') {
                    const project_name = this.viewName(range, id);
                    if (account.account_type === 'billable' && account.by_project[project_name]) {
                        data.billable -= account.by_project[project_name] || 0;
                    } else if (account.account_type === 'non_billable' && account.by_project[project_name]) {
                        data.non_billable -= account.by_project[project_name] || 0;
                    } else if (account.account_type === 'non_billable_responsible' && account.by_project[project_name]) {
                        data.non_billable_responsible -= account.by_project[project_name] || 0;
                    }
                } else if (range === 'user_board') {
                    if (account.account_type === 'billable' && account.by_person[id]) {
                        data.billable -= account.by_person[id] || 0;
                    } else if (account.account_type === 'non_billable' && account.by_person[id]) {
                        data.non_billable -= account.by_person[id] || 0;
                    } else if (account.account_type === 'non_billable_responsible' && account.by_person[id]) {
                        data.non_billable_responsible -= account.by_person[id] || 0;
                    }
                } else {
                    // For other cases, we subtract from the overall
                    if (account.account_type === 'billable') {
                        data.billable -= account.overall || 0;
                    } else if (account.account_type === 'non_billable') {
                        data.non_billable -= account.overall || 0;
                    } else if (account.account_type === 'non_billable_responsible') {
                        data.non_billable_responsible -= account.overall || 0;
                    }
                }
            }
        })

        data.sustainability_target = this.sustainabilityTarget(range, id);
        data.non_billable_total = data.non_billable + data.non_billable_responsible;
        data.total = data.billable + data.non_billable_total;
        data.total_ratio = data.non_billable_total / data.total * 100;
        data.remaining = data.billable * data.sustainability_target / (1 - data.sustainability_target) - data.non_billable_total;
        return data;
    }

    sustainabilityTarget(range, id) {
        let sustainability_targets = this.props.sustainability.sustainability_targets;

        // Set the target using the cell name or company name.
        // In case of user_board, fall back to the company's target.
        return (
            ((range === 'user_board') ? sustainability_targets[COMPANY_NAME] : sustainability_targets[this.viewName(range, id)]) ||
            // use the default vaule in case company or cell value isn't available
            sustainability_targets['default']
        );
    }

    viewName(range, id) {
        if (range === 'board') {
            return this.props.sprints.cells[id];
        } else if (range === 'user_board') {
            return id;
        }
        return COMPANY_NAME;
    }

    handleStartDateChange = date => {
        sessionStorage.setItem("startDate", JSON.stringify(date));
        localStorage.setItem("startDate", JSON.stringify(date));
        // Invoke `loadAccounts` as a callback, because `setState` is asynchronous.
        this.setState({startDate: date}, this.loadAccounts);
    };

    handleEndDateChange = date => {
        sessionStorage.setItem("endDate", JSON.stringify(date));
        localStorage.setItem("endDate", JSON.stringify(date));
        // Invoke `loadAccounts` as a callback, because `setState` is asynchronous.
        this.setState({endDate: date}, this.loadAccounts);
    };

    render() {
        const view = JSON.parse(sessionStorage.getItem("view")) || {'name': 'cells'};
        const {name, id} = view;
        const {accountsLoading, accounts, budgets} = this.props.sustainability;
        const data = this.prepareData(accounts, budgets, name, id);
        const view_name = this.viewName(name, id);

        return (
            <div className='sustainability'>
                <h2>
                    <a href={SUSTAINABILITY_DASHBOARD_DOCS} target='_blank' rel='noopener noreferrer'>
                        Sustainability
                    </a> of {view_name}
                </h2>
                From: &nbsp;
                <DatePicker
                    selected={this.state.startDate}
                    startDate={this.state.startDate}
                    endDate={this.state.endDate}
                    selectsStart
                    dateFormat="yyyy/MM/dd"
                    onChange={this.handleStartDateChange}
                /> &ensp;
                To: &nbsp;
                <DatePicker
                    selected={this.state.endDate}
                    startDate={this.state.startDate}
                    endDate={this.state.endDate}
                    selectsEnd
                    dateFormat="yyyy/MM/dd"
                    onChange={this.handleEndDateChange}
                />

                {
                    // Is data present + logical implication for checking whether the cell is loaded.
                    Object.keys(data).length && (name !== 'board' || this.props.sprints.boards[id])
                        ? <div>
                            {
                                accountsLoading
                                    ? <div className="loading">
                                        <div className="spinner-border"/>
                                        <p>You are viewing the cached version now. The dashboard is being reloaded…</p>
                                    </div>
                                    : <div/>
                            }
                            <SustainabilityTable accounts={data} view={name}/>
                        </div>
                        : <div>
                            <div className="spinner-border"/>
                            <p>Loading the dashboard…</p>
                        </div>
                }
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        auth: state.auth,
        sprints: state.sprints,
        sustainability: state.sustainability,
        selectedAccounts: state.sustainability.selectedAccounts,
    }
};

const mapDispatchToProps = dispatch => {
    return {
        loadUser: () => {
            return dispatch(auth.loadUser());
        },
        loadAccounts: (from, to) => {
            return dispatch(sustainability.loadAccounts(from, to));
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(SustainabilityBoard);
