.DEFAULT_GOAL := help
HELP_SPACING ?= 30
SHELL ?= /bin/bash
PACKAGE_NAME := sprintcraft
PY := python3
PIP := $(PY) -m pip
PY_MANAGE := $(PY) manage.py
DEV_DOCKERFILE := local.yml
SOURCE_TAG ?= $(TAG)

# Parameters ##################################################################
#
# For `test.one` use the rest as arguments and turn them into do-nothing targets
ifeq ($(firstword $(MAKECMDGOALS)),$(filter $(firstword $(MAKECMDGOALS)),test.one dev manage))
  RUN_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  $(eval $(RUN_ARGS):;@:)
endif


# Commands ####################################################################
help: ## Display this help message.
	@echo "Please use \`make <target>' where <target> is one of"
	@perl -nle'print $& if m{^[\.a-zA-Z_-]+:.*?## .*$$}' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m  %-$(HELP_SPACING)s\033[0m %s\n", $$1, $$2}'

manage: ## Run a management command.
	$(PY_MANAGE)

manage.%: ## Run a management command.
	$(PY_MANAGE) "$*"

# Dev commands ################################################################
sh: ## Enter the Django container shell.
	docker compose -f $(DEV_DOCKERFILE) exec django /entrypoint bash

sh.%: ## Run a command in Django container.
	echo "$*"
	docker compose -f $(DEV_DOCKERFILE) exec django /entrypoint bash -c "$*"

up: ## Start all containers.
	docker compose -f $(DEV_DOCKERFILE) up -d

stop: ## Stop all containers.
	docker compose -f $(DEV_DOCKERFILE) stop

dev.%: ## Run a Make command within the Django container.
	make sh."make $*"

build: ## Build a development image.
	DOCKER_BUILDKIT=1 docker build -t sprintcraft_local_django -f ./compose/local/django/Dockerfile .

# Management commands #########################################################
python: manage.shell_plus ## Enter Django shell.
migrate: manage.migrate ## Apply migrations.
migrations: manage.makemigrations ## Prepare migrations for new models.
migrations.check:  ## Check for unapplied migrations.
	!((make manage.showmigrations | grep '\[ \]') && printf "\n\033[0;31mERROR: Pending migrations found\033[0m\n\n")


# Tests #######################################################################
test.quality: clean ## Run quality tests.
	ruff .

quality.fix: clean ## Run quality tests.
	ruff --fix-only .

test.typing: clean ## Run typing tests.
	mypy -p $(PACKAGE_NAME) --junit-xml report_mypy.xml

test.pre-commit: ## Run pre-commit tests.
	pre-commit run --all

test.unit: clean ## Run all unit tests.
	coverage run -m pytest --junitxml=report_pytest.xml
	@coverage report
	@coverage xml -o report_coverage.xml

test.migrations: clean ## Check if migrations are missing.
	@$(PY_MANAGE) makemigrations --dry-run --check

test: clean test.pre-commit test.quality test.typing test.migrations test.unit cov.html ## Run all tests.
	@echo "\nAll tests OK!\n"

cov.html: ## Generate html coverage
	coverage html
	@echo -e "\nCoverage HTML report at file://`pwd`/build/coverage/index.html\n"
	@coverage report

# Cleanup #####################################################################
clean: cov.clean ## Remove all temporary files.
	find -name '*.pyc' -delete
	find -name '*~' -type f -not -path '*/frontend/*' -delete
	find -name '__pycache__' -type d -delete
	rm -rf \
		.coverage \
		.ipython \
		.mypy_cache \
		.pytest_cache \
		build \
		celerybeat.pid \
		htmlcov \
		staticfiles \
		report*.xml

cov.clean:  ## Remove coverage files.
	coverage erase

# CI/CD #######################################################################
ci-test-tag-exists:
	@curl -s "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/registry/repositories?tags=true" | jq -e '.[] | select(.location==env.CI_REGISTRY_IMAGE) | .tags[] | select(.name==env.VERSION)' > /dev/null && \
	echo "Image version ${VERSION} already exists. Please bump up the version by incrementing the 'VERSION' variable in '.gitlab-ci.yml'." && exit 1 || exit 0

ci-build-images:
	docker build \
		--build-arg BUILDKIT_INLINE_CACHE=1 \
		--cache-from "${CI_REGISTRY_IMAGE}/intermediary:$(TAG)" \
		--cache-from "${CI_REGISTRY_IMAGE}/intermediary:latest" \
		--target python-build-stage \
		-t "${CI_REGISTRY_IMAGE}/intermediary:$(TAG)" \
		-t "${CI_REGISTRY_IMAGE}/intermediary:latest" \
		-f ./compose/production/django/Dockerfile .
	docker build \
		--build-arg BUILDKIT_INLINE_CACHE=1 \
		--cache-from "${CI_REGISTRY_IMAGE}/intermediary:latest" \
		--cache-from "${CI_REGISTRY_IMAGE}$(IMAGE):$(TAG)" \
		--cache-from "${CI_REGISTRY_IMAGE}$(IMAGE):latest" \
		-t "${CI_REGISTRY_IMAGE}$(IMAGE):$(TAG)" \
		-t "${CI_REGISTRY_IMAGE}$(IMAGE):latest" \
		-f ./compose/production/django/Dockerfile .

ci-push-images:
	docker push "${CI_REGISTRY_IMAGE}$(IMAGE):$(TAG)"
	docker push "${CI_REGISTRY_IMAGE}/intermediary:$(TAG)"

verify-ci:
	echo "${CI_REGISTRY_IMAGE}$(IMAGE):$(TAG)"
	echo "${CI_REGISTRY_IMAGE}/intermediary:$(TAG)"
