import pytest

from sprintcraft.users.models import User
from sprintcraft.users.tests.test_factories import UserFactory


@pytest.fixture(autouse=True)
def _media_storage(settings, tmpdir):
    settings.MEDIA_ROOT = tmpdir.strpath


@pytest.fixture()
def user(db) -> User:  # pylint: disable=unused-argument
    """Create a pytext fixture for users."""
    return UserFactory()
