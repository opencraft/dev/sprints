from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class DashboardConfig(AppConfig):
    name = 'sprintcraft.dashboard'
    verbose_name = _("Dashboard")

    def ready(self):
        try:  # noqa: SIM105
            import sprintcraft.users.signals  # noqa: F401
        except ImportError:
            pass
