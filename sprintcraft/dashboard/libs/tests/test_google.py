import re
from unittest.mock import Mock, patch

import pytest
from django.conf import settings

from sprintcraft.dashboard.libs.google import get_rotations_allocations, get_rotations_users
from sprintcraft.dashboard.libs.tests.fixtures import allocations_spreadsheet, rotations_spreadsheet


@pytest.mark.parametrize(
    ('calendar_event', 'expected_name', 'expected_action', 'expected_hours'),
    [
        ('John off', 'John', 'off', None),  # Deprecated format.
        ('John Doe off', 'John', 'Doe off', None),  # Deprecated format.
        ('John 3h', 'John', '', '3'),  # Deprecated format.
        ('John available 3h/day', 'John', 'available ', '3'),  # Deprecated format.
        ('John: off', 'John', 'off', None),
        ('John Doe: off', 'John Doe', 'off', None),
        ('John Doe: 3h', 'John Doe', '', '3'),
        ('John Doe: available 3h/day', 'John Doe', 'available ', '3'),
    ],
)
def test_vacation_format(calendar_event, expected_name, expected_action, expected_hours):
    """Test calendar events are correctly parsed through configured regex."""
    regex = settings.GOOGLE_CALENDAR_VACATION_REGEX
    search = re.match(regex, calendar_event, re.IGNORECASE).groupdict()  # type: ignore

    assert search.get('name') or search.get('first_name') == expected_name
    assert search.get('action') == expected_action
    assert search.get('hours') == expected_hours


@patch('sprintcraft.dashboard.libs.google.get_rotations_spreadsheet', return_value=rotations_spreadsheet)
@pytest.mark.parametrize(
    ('sprint', 'cell', 'expected'),
    [
        ('252', 'C1', {'FF': ['C1 M1', 'C1 M2'], 'DD': ['C1 M4', 'C1 M3']}),
        ('253', 'C1', {'FF': ['C1 M3', 'C1 M4'], 'DD': ['C1 M2', 'C1 M1']}),
        ('254', 'C1', {'FF': [], 'DD': []}),
        ('252', 'C2', {'FF': [], 'DD': []}),
        ('253', 'C2', {'FF': ['C2 M1', 'C2 M2'], 'DD': []}),
        ('254', 'C2', {'FF': ['C2 M3', 'C2 M4'], 'DD': []}),
        ('252', 'C3', {'FF': ['C3 M1'], 'DD': ['C3 M3']}),
        ('253', 'C3', {'FF': ['C3 M2'], 'DD': []}),
        ('254', 'C3', {'FF': [], 'DD': []}),
    ],
)
def test_get_rotations_users(get_rotations: Mock, sprint: str, cell: str, expected: dict[str, list[str]]):
    """Test rotations are correctly fetched from the spreadsheet."""
    get_rotations()
    assert get_rotations_users(sprint, cell) == expected


@patch('sprintcraft.dashboard.libs.google.get_rotations_allocations_spreadsheet', return_value=allocations_spreadsheet)
@pytest.mark.parametrize(
    ('cell', 'expected'),
    [
        ('C1', {'FF': {'a': 10, 'b': 20, 'c': 30}, 'DD': {'a': 5, 'b': 6, 'c': 7}}),
        ('C2', {'FF': {'a': 15, 'b': 0, 'c': 17}, 'DD': {'a': 1, 'b': 0, 'c': 3}}),
        ('C3', {'FF': {'a': 50, 'b': 60, 'c': 70}, 'DD': {'a': 0, 'b': 0, 'c': 0}}),
    ],
)
def test_get_rotations_allocations(
    get_rotations_allocations_patch: Mock,
    cell: str,
    expected: dict[str, dict[str, int]],
):
    """Test rotation allocations are fetched correctly from the spreadsheet."""
    get_rotations_allocations_patch()
    assert get_rotations_allocations(cell) == expected
