import re
from collections.abc import Iterator
from contextlib import contextmanager
from datetime import timedelta

from dateutil.parser import parse
from django.conf import settings
from google.oauth2 import service_account
from googleapiclient import discovery
from googleapiclient.http import MediaFileUpload

from config.settings.base import SECONDS_IN_HOUR


@contextmanager
def connect_to_google(service_name: str) -> Iterator[discovery.Resource]:
    """Connect to Google API with service account."""
    scopes = [
        'https://www.googleapis.com/auth/calendar',
        'https://www.googleapis.com/auth/spreadsheets',
        'https://www.googleapis.com/auth/drive.file',
    ]
    credentials = service_account.Credentials.from_service_account_info(settings.GOOGLE_API_CREDENTIALS, scopes=scopes)
    api_version = {
        'calendar': 'v3',
        'sheets': 'v4',
        'drive': 'v3',
    }
    try:
        service = discovery.build(
            service_name,
            api_version[service_name],
            credentials=credentials,
            cache_discovery=False,
        )
    except KeyError as e:
        raise AttributeError("Unknown service name.") from e
    yield service


def get_vacations(from_: str, to: str) -> list[dict[str, int | str | dict[str, str]]]:
    """
    Retrieve user's vacations from Google Calendar.

    The events contain `seconds` key, which indicates user's availability for the period specified in the event.
    """
    with connect_to_google('calendar') as conn:
        calendars = [item['id'] for item in conn.calendarList().list(fields='items(id)').execute()['items']]
        vacations = []
        for calendar in calendars:
            events = (
                conn.events()
                .list(
                    calendarId=calendar,
                    timeZone='Europe/London',
                    timeMin=f'{from_}T00:00:00Z',
                    timeMax=f'{to}T23:59:59Z',
                    fields='items(end/date, start/date, summary)',
                )
                .execute()
            )

            for event in events['items']:
                try:
                    regex = settings.GOOGLE_CALENDAR_VACATION_REGEX
                    search = re.match(regex, event['summary'], re.IGNORECASE).groupdict()  # type: ignore
                    # Only `All day` events are taken into account.
                    if {'start', 'end'} <= event.keys():
                        # The end date of the `All day` event includes a day after that event. The assumption is that we
                        # want to count only inclusive dates, so this "subtracts" one day from the event.
                        event["end"]["date"] = (parse(event["end"]["date"]) - timedelta(days=1)).strftime(
                            settings.JIRA_API_DATE_FORMAT,
                        )
                        user = search.get('name') or search.get('first_name')
                        event['user'] = user
                        event['seconds'] = int(search.get('hours', 0) or 0) * SECONDS_IN_HOUR
                        vacations.append(event)
                except (AttributeError, TypeError):  # The event doesn't relate to vacations.
                    pass

        vacations.sort(key=lambda x: x['user'])  # Small optimization for searching
        return vacations


def upload_spillovers(spillovers: list[list[str]]) -> None:
    """Upload the prepared rows to Google spreadsheet."""
    with connect_to_google('sheets') as conn:
        sheet = conn.spreadsheets()
        body = {'values': spillovers}
        sheet.values().append(
            spreadsheetId=settings.GOOGLE_SPILLOVER_SPREADSHEET,
            range='Spillovers',
            body=body,
            valueInputOption='USER_ENTERED',
        ).execute()


def get_commitments_spreadsheet(cell_name: str) -> list[list[str]]:
    """Retrieve the current commitment spreadsheet."""
    with connect_to_google('sheets') as conn:
        sheet = conn.spreadsheets()
        return (
            sheet.values()
            .get(
                spreadsheetId=settings.GOOGLE_SPILLOVER_SPREADSHEET,
                range=f"'{cell_name} Commitments'!A3:ZZZ999",
                majorDimension='COLUMNS',
            )
            .execute()['values']
        )


def upload_commitments(users: list[str], commitments: list[str], range_: str) -> None:
    """Upload new members and commitments to the spreadsheet."""
    with connect_to_google('sheets') as conn:
        sheet = conn.spreadsheets()
        users_body = {
            'values': [users],
            'majorDimension': 'COLUMNS',
        }
        sheet.values().append(
            spreadsheetId=settings.GOOGLE_SPILLOVER_SPREADSHEET,
            range=range_.split('!')[0],
            body=users_body,
            valueInputOption='USER_ENTERED',
        ).execute()

        body = {'values': [commitments], 'majorDimension': 'COLUMNS'}
        sheet.values().append(
            spreadsheetId=settings.GOOGLE_SPILLOVER_SPREADSHEET,
            range=range_,
            body=body,
            valueInputOption='USER_ENTERED',
        ).execute()


def get_rotations_spreadsheet() -> list[list[str]]:
    """Retrieve the current rotations spreadsheet."""
    with connect_to_google('sheets') as conn:
        sheet = conn.spreadsheets()
        return (
            sheet.values()
            .get(
                spreadsheetId=settings.GOOGLE_ROTATIONS_SPREADSHEET,
                range=settings.GOOGLE_ROTATIONS_RANGE,
                majorDimension='COLUMNS',
            )
            .execute()['values']
        )


def get_rotations_users(sprint_number: str, cell_name: str) -> dict[str, list[str]]:
    """Retrieve users that have cell roles assigned for the chosen sprint."""
    spreadsheet = get_rotations_spreadsheet()
    sprint_rows: list[int] = []
    for i, row in enumerate(spreadsheet[0]):
        if row.startswith(sprint_number):
            sprint_rows.append(i)

    result: dict[str, list[str]] = {}
    for column in spreadsheet:
        if column[0].startswith(cell_name):
            role_name = column[0].replace(cell_name, '').strip()
            result[role_name] = []
            for row in sprint_rows:  # type: ignore
                if row < len(column) and column[row]:  # type: ignore
                    result[role_name].append(column[row])  # type: ignore

    return result


def get_rotations_allocations_spreadsheet() -> list[list[str]]:
    """Retrieve the rotation allocations spreadsheet."""
    with connect_to_google('sheets') as conn:
        sheet = conn.spreadsheets()
        return (
            sheet.values()
            .get(
                spreadsheetId=settings.GOOGLE_ROTATIONS_SPREADSHEET,
                range=settings.GOOGLE_ALLOCATION_RANGE,
                majorDimension='COLUMNS',
            )
            .execute()['values']
        )


def get_rotations_allocations(cell_name: str):
    """Retrieve rotation allocations that has been configured for the cell."""
    spreadsheet = get_rotations_allocations_spreadsheet()
    result: dict[str, dict[str, float]] = {}
    for column in spreadsheet:
        if column[0].startswith(cell_name):
            role_name = column[0].replace(cell_name, '').strip()
            sprint_allocations: dict[str, float] = {}
            for i, row in enumerate(spreadsheet[0]):
                if i != 0:
                    try:
                        sprint_allocations[row] = float(column[i]) if i < len(column) and column[i] else 0
                    except ValueError:
                        sprint_allocations[row] = 0
            result[role_name] = sprint_allocations
    return result


def get_availability_spreadsheet() -> list[list[str]]:
    """Retrieve the availability spreadsheet."""
    with connect_to_google('sheets') as conn:
        sheet = conn.spreadsheets()
        return (
            sheet.values()
            .get(
                spreadsheetId=settings.GOOGLE_CONTACT_SPREADSHEET,
                range=settings.GOOGLE_AVAILABILITY_RANGE,
                majorDimension='ROWS',
            )
            .execute()['values']
        )


def search_drive(name: str, content_type: str = "folder", parent_id: str | None = None) -> str | None:
    """
    Search drive for file/folder of given name under given parent_id.

    Args:
        name: name of the resource
        content_type: optional, default to 'folder'
        parent_id: optional, default to None, if provided it will search inside parent folder

    Returns:
        returns id of the first resource found else None
    """
    search_query = f"name = '{name}'"

    # build query as documented here: https://developers.google.com/drive/api/guides/search-files
    if content_type == "folder":
        search_query = search_query + "and mimeType = 'application/vnd.google-apps.folder'"
    if parent_id:
        search_query = search_query + f" and '{parent_id}' in parents"

    with connect_to_google("drive") as conn:
        page_token = None
        while True:
            response = (
                conn.files()
                .list(
                    q=search_query,
                    spaces="drive",
                    fields="nextPageToken, files(id, name)",
                    pageToken=page_token,
                )
                .execute()
            )
            for file in response.get('files', []):
                # return id if file is found
                return file.get("id")
            page_token = response.get('nextPageToken', None)
            if page_token is None:
                # no more results
                break
    return None


def create_folder(folder_name: str, parent: str) -> str:
    """
    Create a folder in google drive under given parent.

    Args:
        folder_name (str): folder name to create in drive
        parent (str): parent folder id

    Returns:
        id of the newly created folder in drive
    """
    file_metadata = {"name": folder_name, "parents": [parent], "mimeType": "application/vnd.google-apps.folder"}
    with connect_to_google("drive") as conn:
        folder = conn.files().create(body=file_metadata, fields="id").execute()
        return folder.get("id")


def upload_to_drive(file_path: str, filename: str, drive_folder: str) -> str:
    """
    Upload local file to drive in drive_folder.

    Args:
        file_path (str): local file path
        filename (str): file name in google drive
        drive_folder (str): folder id in google drive

    Returns:
        Drive url of uploaded file
    """
    file_metadata = {"name": filename, "parents": [drive_folder]}
    with connect_to_google("drive") as conn:
        media_body = MediaFileUpload(
            file_path,
            mimetype="video/mp4",
        )
        drive_file = (
            conn.files()
            .create(
                body=file_metadata,
                media_body=media_body,
                fields="id",
                supportsAllDrives=True,
            )
            .execute()
        )
        return drive_file.get("id")
