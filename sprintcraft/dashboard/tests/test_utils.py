import inspect
import re
from collections import defaultdict
from pathlib import Path
from unittest import TestCase
from unittest.mock import MagicMock, Mock, call, patch

import pytest
from django.conf import settings
from django.test import override_settings
from jira import User as JiraUser
from jira.resources import Issue, Sprint

from sprintcraft.dashboard.tests.test_helpers import MockItem, does_not_raise
from sprintcraft.dashboard.utils import (
    NoRolesFoundError,
    _column_number_to_excel,
    _extract_sprint_start_date_from_sprint_name,
    _get_sprint_meeting_day_division_for_member,
    compile_participants_roles,
    create_sprints,
    extract_sprint_id_from_str,
    extract_sprint_name_from_str,
    get_all_sprints,
    get_cell,
    get_cell_member_roles,
    get_cell_members,
    get_cells,
    get_current_sprint,
    get_issue_fields,
    get_next_sprint,
    get_projects_dict,
    get_rotations_roles_for_member,
    get_spillover_reason,
    get_sprint_end_date,
    get_sprint_number,
    get_sprint_start_date,
    get_sprints,
    prepare_jql_query,
    prepare_jql_query_active_sprint_tickets,
    prepare_spillover_rows,
)


class MockJiraConnection:
    @staticmethod
    def boards(name=''):
        boards = [
            MockItem(id=1, name=f'{settings.JIRA_SPRINT_BOARD_PREFIX}Test1'),
            MockItem(id=2, name=f'{settings.JIRA_SPRINT_BOARD_PREFIX}Test2'),
            MockItem(id=3, name='Test3'),
        ]
        if name:
            # noinspection PyUnresolvedReferences
            return [board for board in boards if board.name.startswith(settings.JIRA_SPRINT_BOARD_PREFIX)]
        return boards

    @staticmethod
    def fields():
        return [
            {
                'name': 'example_name1',
                'id': 'example_id1',
            },
            {
                'name': 'example_name2',
                'id': 'example_id2',
            },
            {
                'name': 'example_name3',
                'id': 'example_id3',
            },
        ]

    @property
    def issue_fields(self):
        return {field['name']: field['id'] for field in self.fields()}

    @staticmethod
    def sprints(board_id, **_kwargs):
        if board_id == 1:
            return [
                MockItem(id=4, name='T1.125 (2019-01-01)', state='future'),
                MockItem(id=1, name='T1.123 (2019-01-01)', state='active'),
            ]
        return [
            MockItem(id=2, name='T2.124 (2019-01-01)', state='future'),
            MockItem(id=3, name='T1.124 (2019-01-01)', state='future'),
        ]

    @staticmethod
    def projects():
        return [
            MockItem(name='Test1', key='T1'),
            MockItem(name='Test2', key='T2'),
            MockItem(name='Test3', key='T3'),
        ]


class MockUser:
    JANE = MockItem(displayName='Jane Doe', emailAddress='janedoe@opencraft.com')
    JACK = MockItem(displayName='Jack Doe', emailAddress='jackdoe@opencraft.com')
    JOHN = MockItem(displayName='John Doe', emailAddress='johndoe@opencraft.com')
    JAKE = MockItem(displayName='Jake Doe', emailAddress='jakedoe@opencraft.com')

    @classmethod
    def list(cls) -> list[JiraUser]:
        return [user for (_, user) in inspect.getmembers(cls, lambda x: isinstance(x, MockItem))]


def test_get_projects_dict():
    # noinspection PyTypeChecker
    projects = get_projects_dict(MockJiraConnection())
    expected = {
        'Test1': MockItem(name='Test1', key='T1'),
        'Test2': MockItem(name='Test2', key='T2'),
        'Test3': MockItem(name='Test3', key='T3'),
    }
    assert projects == expected


def test_get_cells():
    # noinspection PyTypeChecker
    cells = get_cells(MockJiraConnection())
    assert len(cells) == 2
    assert cells[0].name == 'Test1'
    assert cells[0].board_id == 1
    assert cells[0].key == 'T1'
    assert cells[1].name == 'Test2'
    assert cells[1].board_id == 2
    assert cells[1].key == 'T2'


@pytest.mark.parametrize(
    ('test_input', 'expected', 'raises'),
    [
        (1, 'T1', does_not_raise()),
        (2, 'T2', does_not_raise()),
        (3, 'T3', pytest.raises(ValueError, match='Cell not found.')),
    ],
)
def test_get_cell_key(test_input, expected, raises):
    with raises:
        # noinspection PyTypeChecker
        assert get_cell(MockJiraConnection(), test_input).key == expected


def test_get_cell_members():
    quickfilters = [
        MockItem(query='assignee = Test1 or reviewer_1 = Test1 or reviewer_2 = Test1'),
        MockItem(query='assignee = Test2'),
        MockItem(query='Test3'),
    ]
    # noinspection PyTypeChecker
    members = get_cell_members(quickfilters)
    assert len(members) == 1
    assert members[0] == 'Test1'


@pytest.mark.parametrize(
    ('cell_key', 'expected_number'),
    [
        ('', 3),
        ('T1', 2),
        ('T2', 1),
    ],
)
def test_get_sprints(cell_key: str, expected_number: int):
    """`get_sprints` retrieves sprints from Jira API and allows filtering them by cell."""
    sprints = [
        MockItem(name='T1.125 (2019-01-01)', state='future'),
        MockItem(name='T2.124 (2019-01-01)', state='future'),
        MockItem(name='T1.124 (2019-01-01)', state='future'),
        MockItem(name='MNG.124 (2019-01-01)', state='future'),
    ]
    mock_jira_sprints = Mock(return_value=sprints)
    conn = Mock(sprints=mock_jira_sprints)
    board_id = 123

    result = get_sprints(conn, board_id, cell_key=cell_key)

    mock_jira_sprints.assert_called_once_with(board_id, state='active, future')
    assert len(result) == expected_number


def test_get_sprint_number():
    sprint = MockItem(name='T1.123 (2019-01-01)', state='active')
    # noinspection PyTypeChecker
    assert get_sprint_number(sprint) == 123


def test_get_next_sprint():
    sprints = [
        MockItem(name='T1.125 (2019-01-01)', state='future'),
        MockItem(name='T1.123 (2019-01-01)', state='active'),
        MockItem(name='T1.124 (2019-01-01)', state='future'),
    ]
    # noinspection PyTypeChecker
    assert get_next_sprint(sprints, sprints[1]) == sprints[2]

    # noinspection PyTypeChecker
    assert get_next_sprint(sprints, sprints[2]) == sprints[0]

    # noinspection PyTypeChecker
    assert get_next_sprint(sprints, sprints[0]) is None  # Next sprint not found


def test_get_all_sprints():
    # noinspection PyTypeChecker
    sprints = get_all_sprints(MockJiraConnection())
    expected = {
        'active': [
            MockItem(id=1, name='T1.123 (2019-01-01)', state='active'),
        ],
        'future': [
            MockItem(id=2, name='T2.124 (2019-01-01)', state='future'),
            MockItem(id=3, name='T1.124 (2019-01-01)', state='future'),
        ],
        'all': [
            MockItem(id=4, name='T1.125 (2019-01-01)', state='future'),
            MockItem(id=1, name='T1.123 (2019-01-01)', state='active'),
            MockItem(id=2, name='T2.124 (2019-01-01)', state='future'),
            MockItem(id=3, name='T1.124 (2019-01-01)', state='future'),
        ],
    }
    assert sprints == expected


def test_get_all_sprints_with_cell_specified():
    # noinspection PyTypeChecker
    sprints = get_all_sprints(MockJiraConnection(), 1)
    expected = {
        'active': [
            MockItem(id=1, name='T1.123 (2019-01-01)', state='active'),
        ],
        'future': [],
        'cell': [
            MockItem(id=4, name='T1.125 (2019-01-01)', state='future'),
            MockItem(id=1, name='T1.123 (2019-01-01)', state='active'),
            MockItem(id=3, name='T1.124 (2019-01-01)', state='future'),
        ],
        'all': [
            MockItem(id=4, name='T1.125 (2019-01-01)', state='future'),
            MockItem(id=1, name='T1.123 (2019-01-01)', state='active'),
            MockItem(id=2, name='T2.124 (2019-01-01)', state='future'),
            MockItem(id=3, name='T1.124 (2019-01-01)', state='future'),
        ],
    }
    assert sprints == expected


@patch("sprintcraft.dashboard.utils._extract_sprint_start_date_from_sprint_name")
def test_get_sprint_start_date(mock: MagicMock):
    # noinspection PyTypeChecker
    sprint = MockItem(name='T1.123 (2019-01-01)', state='active')  # type: Sprint
    get_sprint_start_date(sprint)
    mock.assert_called_once_with(sprint.name)


def test_sprint_end_date():
    # noinspection PyTypeChecker
    sprint = MockItem(name='T1.123 (2019-01-01)', state='active')  # type: Sprint
    assert get_sprint_end_date(sprint) == '2019-01-14'


@patch(
    "sprintcraft.dashboard.utils.get_all_sprints",
    return_value={
        'active': [MockItem(name="BB.279"), MockItem(name="SE.279")],
        'all': [MockItem(name="BB.279"), MockItem(name="SE.279")],
    },
)
@patch("sprintcraft.dashboard.libs.jira.CustomJira")
def test_get_current_sprint(mock_jira: MagicMock, mock_get_all_sprints: MagicMock):
    assert get_current_sprint('active').name == "BB.279"
    mock_get_all_sprints.assert_called_once_with(mock_jira())


@patch("sprintcraft.dashboard.libs.jira.CustomJira")
def test_get_current_sprint_for_cell(mock_jira: MagicMock):
    mock_jira.return_value.sprints.return_value = [MockItem(name="STAR.78"), MockItem(name="BB.279")]
    assert get_current_sprint('active', board_id=24).name == "BB.279"


def test_prepare_jql_query():
    expected_fields = ['id', 'sprint']
    expected_result = {
        'jql_str': r'^\(Sprint IN \(245,246\) AND status IN \(.*?\) OR \(issuetype = Epic AND Status IN \(.*?\)\)$',
        'fields': expected_fields,
    }
    result = prepare_jql_query(
        sprints=['245', '246'],
        fields=expected_fields,
    )
    assert result['fields'] == expected_result['fields']
    assert re.match(expected_result['jql_str'], result['jql_str'])


def test_extract_sprint_id_from_str():
    sprint_str = (
        'com.atlassian.greenhopper.service.sprint.Sprint@614e3007[id=245,rapidViewId=26,state=CLOSED,'
        'name=Sprint 197 (2019-06-18),startDate=2019-06-17T17:21:26.945Z,'
        'endDate=2019-07-01T17:21:00.000Z,completeDate=2019-07-01T17:46:48.977Z,sequence=243,goal=] '
    )
    assert extract_sprint_id_from_str(sprint_str) == 245


def test_prepare_jql_query_active_sprint_tickets():
    expected_fields = ['id']
    expected_result = {
        'jql_str': 'Sprint IN openSprints() AND status IN ("Backlog","In progress","Need Review","Merged")',
        'fields': expected_fields,
    }
    result = prepare_jql_query_active_sprint_tickets(
        expected_fields,
        ("Backlog", "In progress", "Need Review", "Merged"),
    )
    assert result == expected_result


@pytest.mark.parametrize(
    ('test_input', 'expected', 'raises'),
    [
        ('Sprint 201 (2019-08-13)', '2019-08-13', does_not_raise()),
        ('SE.202 (2019-08-27)', '2019-08-27', does_not_raise()),
        ('Sprint 201 (2019-08-13', '', pytest.raises(AttributeError)),
    ],
)
def test_extract_sprint_start_date_from_sprint_name(test_input, expected, raises):
    with raises:
        assert _extract_sprint_start_date_from_sprint_name(test_input) == expected


def test_prepare_jql_query_active_sprint_tickets_for_project_and_unassigned():
    expected_fields = ['id']
    expected_result = {
        'jql_str': (
            'project = TEST AND '
            'Sprint IN openSprints() AND '
            'status IN ("Backlog","In progress") AND '
            'assignee = null'
        ),
        'fields': expected_fields,
    }
    result = prepare_jql_query_active_sprint_tickets(
        expected_fields,
        ("Backlog", "In progress"),
        project="TEST",
        is_unassigned=True,
    )
    assert result == expected_result


@pytest.mark.parametrize(
    ('test_input', 'expected'),
    [
        ('name=Sprint 197 (2019-06-18),startDate=2019-06-17T17:21:26.945Z', 'Sprint 197 (2019-06-18)'),
        ('name=TS.197 (2019-06-18),startDate=2019-06-17T17:21:26.945Z', 'TS.197 (2019-06-18)'),
    ],
)
def test_extract_sprint_name_from_str(test_input, expected):
    assert extract_sprint_name_from_str(test_input) == expected


def test_get_issue_fields():
    required_fields = ('example_name1', 'example_name2')
    expected_result = {
        required_fields[0]: 'example_id1',
        required_fields[1]: 'example_id2',
    }
    # noinspection PyTypeChecker
    assert get_issue_fields(MockJiraConnection(), required_fields) == expected_result


@patch("sprintcraft.dashboard.utils.get_cell", return_value=Mock(key="T1", board_id=123))
@patch(
    "sprintcraft.dashboard.utils.get_sprints",
    return_value=[
        MockItem(id=1, name='T1.123 (2019-01-01)', state='active'),
        MockItem(id=3, name='T1.124 (2019-01-15)', state='future'),
        MockItem(id=4, name='T1.125 (2019-01-29)', state='future'),
    ],
)
@pytest.mark.parametrize('number_of_sprints', [1, 0])
def test_create_sprints(mock_get_sprints: MagicMock, mock_get_cell: MagicMock, number_of_sprints: int):
    """Test that the `create_sprints` function makes correct API calls."""
    mock_jira_create_sprint = Mock(side_effect=lambda name, **_kwargs: MockItem(id=5, name=name, state='future'))
    conn = Mock(create_sprint=mock_jira_create_sprint)
    cell_key = "T1"
    board_id = 123

    result = create_sprints(board_id, number_of_sprints=number_of_sprints, jira_conn=conn)

    mock_get_cell.assert_called_once_with(conn, board_id)
    mock_get_sprints.assert_called_once_with(conn, board_id, cell_key=cell_key)

    if number_of_sprints:
        assert len(result) == number_of_sprints
        mock_jira_create_sprint.assert_called_once_with(
            name='T1.126 (2019-02-12)',
            board_id=123,
            startDate='2019-02-12',
            endDate='2019-02-25',
        )
    else:
        assert len(result) == settings.SPRINTS_EXPECTED_NUMBER_PER_CELL - len(mock_get_sprints())
        assert mock_jira_create_sprint.call_args_list == [
            call(name='T1.126 (2019-02-12)', board_id=123, startDate='2019-02-12', endDate='2019-02-25'),
            call(name='T1.127 (2019-02-26)', board_id=123, startDate='2019-02-26', endDate='2019-03-11'),
            call(name='T1.128 (2019-03-12)', board_id=123, startDate='2019-03-12', endDate='2019-03-25'),
            call(name='T1.129 (2019-03-26)', board_id=123, startDate='2019-03-26', endDate='2019-04-08'),
        ]


@patch("sprintcraft.dashboard.utils.get_sprint_start_date", return_value="2019-01-01T13:00:00")
@pytest.mark.parametrize(
    ('issue', 'assignee', 'expected'),
    [
        # No spillover reason provided.
        (
            MockItem(
                key='TEST-1',
                fields=MockItem(
                    comment=MockItem(
                        comments=[
                            MockItem(created='2019-01-01T13:00:00Z', body="Test comment 1.", author=MockUser.JANE),
                            MockItem(created='2019-01-01T13:00:01Z', body="Test comment 2.", author=MockUser.JANE),
                            MockItem(created='2019-01-01T13:00:02Z', body="Test comment 3.", author=MockUser.JANE),
                        ],
                    ),
                ),
            ),
            MockUser.JANE,
            "",
        ),
        # Spillover reason provided.
        (
            MockItem(
                key='TEST-1',
                fields=MockItem(
                    comment=MockItem(
                        comments=[
                            MockItem(
                                created='2019-01-01T13:00:00Z',
                                body="[~crafty]: <spillover>Test spillover.</spillover>",
                                author=MockUser.JANE,
                            ),
                            MockItem(created='2019-01-01T13:00:01Z', body="Test comment 2.", author=MockUser.JANE),
                            MockItem(created='2019-01-01T13:00:02Z', body="Test comment 3.", author=MockUser.JANE),
                        ],
                    ),
                ),
            ),
            MockUser.JANE,
            "Test spillover.",
        ),
        # Spillover reason provided by another user.
        (
            MockItem(
                key='TEST-1',
                fields=MockItem(
                    comment=MockItem(
                        comments=[
                            MockItem(
                                created='2019-01-01T13:00:00Z',
                                body="[~crafty]: <spillover>Test spillover.</spillover>",
                                author=MockUser.JANE,
                            ),
                            MockItem(created='2019-01-01T13:00:01Z', body="Test comment 2.", author=MockUser.JANE),
                            MockItem(created='2019-01-01T13:00:02Z', body="Test comment 3.", author=MockUser.JANE),
                        ],
                    ),
                ),
            ),
            MockUser.JACK,
            "",
        ),
        # Wrong pattern for the spillover reason.
        (
            MockItem(
                key='TEST-1',
                fields=MockItem(
                    comment=MockItem(
                        comments=[
                            MockItem(
                                created='2019-01-01T13:00:00Z',
                                body="[~crafty], <spillover>Test spillover.</spillover>",
                                author=MockUser.JANE,
                            ),
                            MockItem(created='2019-01-01T13:00:01Z', body="Test comment 2.", author=MockUser.JANE),
                            MockItem(created='2019-01-01T13:00:02Z', body="Test comment 3.", author=MockUser.JANE),
                        ],
                    ),
                ),
            ),
            MockUser.JANE,
            "",
        ),
        # Spillover reason provided in the previous sprint.
        # As a ticket can spill over multiple times, only the comments from the current sprint should be checked.
        (
            MockItem(
                key='TEST-1',
                fields=MockItem(
                    comment=MockItem(
                        comments=[
                            MockItem(
                                created='2019-01-01T12:59:59Z',
                                body="[~crafty], <spillover>Test spillover.</spillover>",
                                author=MockUser.JANE,
                            ),
                            MockItem(created='2019-01-01T13:00:01Z', body="Test comment 2.", author=MockUser.JANE),
                            MockItem(created='2019-01-01T13:00:02Z', body="Test comment 3.", author=MockUser.JANE),
                        ],
                    ),
                ),
            ),
            MockUser.JANE,
            "",
        ),
    ],
)
def test_get_spillover_reason(mock_get_sprint_start_date: MagicMock, issue: Issue, assignee: JiraUser, expected: str):
    issue_fields = {'Comment': 'comment'}
    # noinspection PyTypeChecker
    sprint: Sprint = None
    assert get_spillover_reason(issue, issue_fields, sprint, assignee.displayName) == expected
    mock_get_sprint_start_date.assert_called_once()


@override_settings(JIRA_SERVER='https://example.com', SPILLOVER_REQUIRED_FIELDS=('Story Points', 'Original Estimate'))
def test_prepare_spillover_rows():
    test_issues = [
        MockItem(key='TEST-1', fields=MockItem(story_points=1.0, original_estimate=7200)),
        MockItem(
            key='TEST-2',
            fields=MockItem(story_points=3.14, original_estimate=9849),  # estimated time should be rounded up here
        ),
    ]
    issue_fields = {
        'Story Points': 'story_points',
        'Original Estimate': 'original_estimate',
    }
    expected_result = [
        ['=HYPERLINK("https://example.com/browse/TEST-1","TEST-1")', '1', '2.0'],
        ['=HYPERLINK("https://example.com/browse/TEST-2","TEST-2")', '3', '2.74'],
    ]
    # noinspection PyTypeChecker
    assert prepare_spillover_rows(test_issues, issue_fields, {}) == expected_result


@pytest.mark.parametrize(
    ('test_input', 'expected'),
    [
        (1, 'A'),
        (26, 'Z'),
        (27, 'AA'),
        (52, 'AZ'),
        (53, 'BA'),
        (702, 'ZZ'),
        (703, 'AAA'),
        (18278, 'ZZZ'),
        (18279, 'AAAA'),
        (214570915, 'RANDOM'),
    ],
)
def test_column_number_to_excel(test_input, expected):
    assert _column_number_to_excel(test_input) == expected


@override_settings(DEBUG=True)  # `sentry_sdk` does not capture exceptions in `DEBUG` mode.
@pytest.mark.parametrize(
    ('hours', 'expected'),
    [
        ("12pm-9pm*", 0),
        ("3pm-12am", 0),
        ("12am-2am", 0),
        ("3pm - 1am", 0.9),
        ("11:30pm-2am", 0.2),
        ("invalid", 0),
    ],
)
def test_get_sprint_meeting_day_division_for_member(hours, expected):
    sprint_start = "2020-01-01"
    assert _get_sprint_meeting_day_division_for_member(hours, sprint_start) == pytest.approx(expected, 0.1)


@patch("requests.get")
def test_get_cell_member_roles(mock_get):
    with Path("sprintcraft/dashboard/tests/data/handbook/organization.yml").open() as f:
        mock_get.return_value = Mock(text=f.read())
        output = dict(get_cell_member_roles())
        expected_output = {
            'Batman': [
                'Sprint Planning Manager',
                'Community Liaison',
                'Developer Advocate',
                'developer',
            ],
            'Frankie the Cat': ['UX Designer', 'Product Manager', 'Cool'],
            'Hunter the Cat': [
                'DevOps Specialist',
                'Developer Advocate',
                'Community Liaison',
                'developer',
            ],
            'Inspector Gadget': [
                'Cell Supporter',
                'Epic Planning and Sustainability Manager',
                'Cool',
            ],
            'Jim Bob': ['developer'],
            'Kylo Ren': ['DevOps Specialist', 'developer'],
            'Walter White': [
                'Business Development Specialist',
                'Sprint Manager',
                'Cool',
            ],
        }

        assert output == expected_output


@patch("requests.get")
def test_get_cell_member_roles_corrupted(mock_get):
    with Path("sprintcraft/dashboard/tests/data/handbook/organization_corrupted.yml").open() as f:
        mock_get.return_value = Mock(text=f.read())
        with TestCase().assertRaises(NoRolesFoundError):
            get_cell_member_roles()


def test_get_rotations_roles_for_member():
    # When the member is on FF duty
    output = get_rotations_roles_for_member(
        'John Doe',
        {'FF': ['Jake Doe', 'John Doe'], 'DD': ['Jane Doe', 'James Doe']},
    )
    assert len(output) == 1
    assert output[0] == 'FF'

    # When the member is on Future Sprint Firefighter(FSFF) duty
    output = get_rotations_roles_for_member(
        'Jake Doe',
        {'FF': ['Jake Doe', 'John Doe'], 'DD': ['Jane Doe', 'James Doe'], 'FSFF': ['John Doe', 'Jake Doe']},
    )
    assert len(output) == 2
    assert output[0] == 'FF'
    assert output[1] == 'FSFF'

    # When member has no duty
    output = get_rotations_roles_for_member(
        'John Doe',
        {'FF': ['Jake Doe', 'James Doe'], 'DD': ['Jane Doe', 'James Doe']},
    )
    assert len(output) == 0

    # When only partial name of the member is defined
    output = get_rotations_roles_for_member(
        'John Doe',
        {'FF': ['John Doe', 'Jake Doe'], 'DD': ['Jane Doe', 'James Doe']},
    )
    assert len(output) == 1
    assert output[0] == 'FF'


def test_compile_participants_roles():
    rotations_data_dummy = {
        'FF': ['John Doe', 'Jack'],
        'DD': ['Jack Doe', 'Jake Doe'],
        'FSFF': ['Jack', 'John Doe'],
        'FSDD': ['Jake Doe', 'Jack Doe'],
    }

    # noinspection PyTypeChecker
    roles_data_dummy: defaultdict[str, list[str | None]] = {
        'Jane Doe': [],
        'Jack Doe': ['Sprint Planning Manager', 'DevOps Specialist'],
        'John Doe': ['Sprint Manager'],
        'Jake Doe': ['Recruitment Manager'],
        'Juan Doe': [''],
    }

    output = compile_participants_roles(MockUser.list(), rotations_data_dummy, roles_data_dummy)

    expected_output = {
        'janedoe@opencraft.com': [],
        'jackdoe@opencraft.com': [
            'Sprint Planning Manager',
            'DevOps Specialist',
            'FF',
            'DD',
            'FSFF',
            'FSDD',
        ],
        'johndoe@opencraft.com': ['Sprint Manager', 'FF', 'FSFF'],
        'jakedoe@opencraft.com': ['Recruitment Manager', 'DD', 'FSDD'],
    }

    assert output == expected_output
