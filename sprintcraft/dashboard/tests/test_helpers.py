from contextlib import contextmanager


@contextmanager
def does_not_raise():
    yield


class MockItem:
    def __init__(self, **kwargs) -> None:
        super().__init__()
        for k, v in kwargs.items():
            setattr(self, k, v)
        self._attrs = kwargs.keys()

    def __eq__(self, other):
        for attr in self._attrs:  # noqa: SIM111
            if getattr(self, attr) != getattr(other, attr):
                return False
        return True

    def __hash__(self) -> int:
        result = 0
        for attr in self._attrs:
            result = hash((result << 1, hash(attr)))
        return result

    def __repr__(self):
        return ', '.join([str(getattr(self, attr)) for attr in self._attrs])
