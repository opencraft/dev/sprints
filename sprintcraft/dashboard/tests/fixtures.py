"""
Fixtures for dashboard tests.
"""

from pathlib import Path

from sprintcraft.dashboard.utils import derive_roles_from_definition

with Path("sprintcraft/dashboard/tests/data/handbook/organization.yml").open() as f:
    ROLES_DICT = derive_roles_from_definition(f.read())
