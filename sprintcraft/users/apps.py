from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class UsersConfig(AppConfig):
    name = "sprintcraft.users"
    verbose_name = _("Users")

    def ready(self):
        try:  # noqa: SIM105
            import sprintcraft.users.signals  # noqa: F401
        except ImportError:
            pass
