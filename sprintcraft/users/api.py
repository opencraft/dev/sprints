from allauth.socialaccount.providers.oauth2.client import OAuth2Client
from dj_rest_auth.registration.views import SocialLoginView

from config.settings.base import FRONTEND_URL

from .adapters import GoogleIdentityServiceOAuth2Adapter


class GoogleLogin(SocialLoginView):
    adapter_class = GoogleIdentityServiceOAuth2Adapter
    callback_url = FRONTEND_URL
    client_class = OAuth2Client
