"""
Django URL configuration for the sustainability app.
"""
from django.urls import path
from rest_framework.routers import DefaultRouter

from sprintcraft.sustainability.views import AccountsViewSet, PopulateBudgetsView, SustainabilityDashboardViewSet

app_name = "sustainability"
router = DefaultRouter()
router.register(r'dashboard', SustainabilityDashboardViewSet, basename='dashboard')
router.register(r'accounts', AccountsViewSet, basename='accounts')

urlpatterns = [
    path('populate_budgets/', PopulateBudgetsView.as_view()),
]
urlpatterns += router.urls  # type: ignore
