from django.contrib import admin
from django.db import models

from sprintcraft.sustainability.models import Account, Budget, Cell
from sprintcraft.sustainability.widgets import MonthYearWidget


@admin.register(Budget)
class BudgetAdmin(admin.ModelAdmin):
    """We want to override datepicker for the `date` field."""

    formfield_overrides = {
        models.DateField: {'widget': MonthYearWidget},
    }
    list_filter = ('account', 'date')
    search_fields = ('account__name',)
    date_hierarchy = 'date'


@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    list_display = ('name', 'is_fixed_price')
    search_fields = ('name',)
    list_filter = ('is_fixed_price',)


@admin.register(Cell)
class CellAdmin(admin.ModelAdmin):
    pass
