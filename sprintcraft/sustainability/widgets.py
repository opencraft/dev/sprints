import datetime

from django.forms.widgets import NumberInput, Select, Widget
from django.utils.dates import MONTHS
from django.utils.safestring import SafeString, mark_safe

__all__ = ('MonthYearWidget',)


class MonthYearWidget(Widget):
    """
    A Widget that splits date input into a <select> box for the month and text field for the year,
    with 'day' defaulting to the first of the month.

    Based on SelectDateWidget, in `django/trunk/django/forms/extras/widgets.py`
    """

    none_value = (0, '---')
    month_field = '%s_month'
    year_field = '%s_year'

    def __init__(self, attrs=None, required=True):
        super().__init__(attrs)
        self.attrs = attrs or {}
        self.required = required

    def render(self, name, value, attrs=None, renderer=None) -> SafeString:
        try:
            year_val, month_val = value.year, value.month
        except AttributeError:
            now = datetime.datetime.now()
            year_val = now.year
            month_val = now.month

        output = []

        if 'id' in self.attrs:  # noqa: SIM108,SIM401
            id_ = self.attrs['id']
        else:
            id_ = f'id_{name}'

        month_choices = list(MONTHS.items())
        if not (self.required and value):
            month_choices.append(self.none_value)
        month_choices.sort()
        local_attrs = self.build_attrs(base_attrs=self.attrs)
        month = Select(choices=month_choices)
        select_html = month.render(self.month_field % name, month_val, local_attrs)
        output.append(select_html)

        local_attrs['id'] = self.year_field % id_
        id_ = NumberInput()
        select_html = id_.render(self.year_field % name, year_val, local_attrs)
        output.append(select_html)

        return mark_safe('\n'.join(output))  # noqa: S308

    def id_for_label(cls, id_) -> str:  # noqa: N805
        return cls.month_field % id_

    id_for_label = classmethod(id_for_label)  # type: ignore

    def value_from_datadict(self, data, files, name) -> str | None:
        y = data.get(self.year_field % name)
        m = data.get(self.month_field % name)
        if y == m == "0":
            return None
        if y and m:
            return f'{y}-{m}-{1}'
        return data.get(name, None)
