from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class SustainabilityConfig(AppConfig):
    name = 'sprintcraft.sustainability'
    verbose_name = _("Sustainability")

    def ready(self):
        try:  # noqa: SIM105
            import sprintcraft.users.signals  # noqa: F401
        except ImportError:
            pass
