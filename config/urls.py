from dj_rest_auth.jwt_auth import get_refresh_view
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.shortcuts import redirect
from django.urls import include, path, re_path
from django.views import defaults as default_views
from drf_spectacular.views import SpectacularAPIView, SpectacularSwaggerView

from sprintcraft.users.api import GoogleLogin

urlpatterns = [
    path("", lambda request: redirect('/api/docs')),
    # Django Admin, use {% url 'admin:index' %}
    path(settings.ADMIN_URL, admin.site.urls),
    # User management
    path("users/", include("sprintcraft.users.urls", namespace="users")),
    # Dashboard
    path("dashboard/", include("sprintcraft.dashboard.urls", namespace="dashboard")),
    # Sustainability Dashboard
    path("sustainability/", include("sprintcraft.sustainability.urls", namespace="sustainability")),
]

# Provide an option to disable standard (not social auth) login/registration page.
if not getattr(settings, "ACCOUNT_ALLOW_LOGIN", True):
    urlpatterns += [
        path(
            "accounts/login/",
            default_views.page_not_found,
            kwargs={"exception": Exception("Page not Found")},
        ),
        path(
            "accounts/signup/",
            default_views.page_not_found,
            kwargs={"exception": Exception("Page not Found")},
        ),
    ]

urlpatterns += [
    path("accounts/", include("allauth.urls")),
    re_path(r'^rest-auth/refresh/', get_refresh_view().as_view(), name='token_refresh'),  # TODO: Delete for deployment.
    path('rest-auth/', include('dj_rest_auth.urls')),
    path('rest-auth/registration/', include('dj_rest_auth.registration.urls')),
    path('rest-auth/google/', GoogleLogin.as_view(), name='google_login'),
    path("api/schema/", SpectacularAPIView.as_view(), name="api-schema"),
    path("api/docs/", SpectacularSwaggerView.as_view(url_name="api-schema"), name="api-docs"),
    *static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT),
]
