# SprintCraft

App for tracking sprint commitments, vacations and spillovers.

[![Built with Cookiecutter Django](https://img.shields.io/badge/built%20with-Cookiecutter%20Django-ff69b4.svg)](https://github.com/pydanny/cookiecutter-django/)

You can find the documentation here: <https://doc.sprintcraft.opencraft.com>.
